/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vegetable;

import vegetable.VegetableFactory.VegetableType;

/**
 *
 * @author Mauricio
 */
public class VegetableSimulation {

    public static void main(String[] args) {
        Vegetable v[] = new Vegetable[4];

        VegetableFactory factory = VegetableFactory.getInstance();
        Vegetable ripeCarrot = factory.getVegetable(VegetableType.CARROT, "orange", 3);
        Vegetable notRipeCarrot = factory.getVegetable(VegetableType.CARROT, "green", 1);
        
        Vegetable ripeBeet = factory.getVegetable(VegetableType.BEET, "red", 3);
        Vegetable notRipeBeet = factory.getVegetable(VegetableType.BEET, "green", 2);
        
        v[0]=ripeCarrot;
        v[1]=notRipeCarrot;
        v[2]=ripeBeet;
        v[3]=notRipeBeet;
        
        for (int i = 0; i < 4; i++) {
            System.out.println(v[i]);
        }
    }
}
