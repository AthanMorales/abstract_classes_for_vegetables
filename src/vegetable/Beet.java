/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vegetable;

/**
 *
 * @author Mauricio
 */
public class Beet extends Vegetable {

    public Beet(String color, double size) {
        super(color, size);
    }

    @Override
    boolean isRipe() {
        if (super.getColor().equals("red") && super.getSize() >= 2) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return "Beet color is " + super.getColor() + " and size is: " + super.getSize() + " is Ripe: " + isRipe();
    }
}
