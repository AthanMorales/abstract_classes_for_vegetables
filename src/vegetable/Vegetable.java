/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vegetable;

/**
 *
 * @author Mauricio
 */
abstract class Vegetable {

    String color;
    double size;

    abstract boolean isRipe();

    public abstract String toString();

    public Vegetable(String color, double size) {
        this.color = color;
        this.size = size;
    }

    public String getColor() {
        return color;
    }

    public double getSize() {
        return size;
    }
}
